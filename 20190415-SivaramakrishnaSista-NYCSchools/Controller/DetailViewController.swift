//
//  DetailViewController.swift
//  20190415-SivaramakrishnaSista-NYCSchools
//
//  Created by Sivaramakrishna Sista on 4/15/19.
//  Copyright © 2019 Sivaramakrishna Sista. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {
    @IBOutlet weak var satMathScore: UILabel!
    @IBOutlet weak var satReadScore: UILabel!
    @IBOutlet weak var satWriteScore: UILabel!
    var satInfo: SatInfo?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = satInfo?.school_name
        
        satMathScore.text = satInfo?.sat_math_avg_score
        satReadScore.text = satInfo?.sat_critical_reading_avg_score
        satWriteScore.text = satInfo?.sat_writing_avg_score
    }
}
