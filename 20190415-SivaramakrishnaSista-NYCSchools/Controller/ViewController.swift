//
//  ViewController.swift
//  20190415-SivaramakrishnaSista-NYCSchools
//
//  Created by Sivaramakrishna Sista on 4/15/19.
//  Copyright © 2019 Sivaramakrishna Sista. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var nycTableView: UITableView!
    var InfoList:[SchoolInfo] = []
    var SatInfoList:[SatInfo] = []
    let nycCellIdentifier = Contants.Identifier.nycTableViewCellIdentifier
   
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.title = Contants.Title.mainViewTitle
        
        // Do any additional setup after loading the view.
        nycTableView.register(UINib(nibName: Contants.NibName.nycTableViewCell, bundle: nil), forCellReuseIdentifier: nycCellIdentifier)
        nycTableView.estimatedRowHeight = 80.0
        nycTableView.rowHeight = UITableView.automaticDimension
        
        //Get Data from api call
        let apiService = ApiService()
        apiService.fetchSchoolInfo(success:  {  [weak self] schoolInfoList in
            self?.InfoList = schoolInfoList
            self?.nycTableView.reloadData()
        }) { error in
            let failureResponse = UIAlertAction(title: "OK", style: .default) {[weak self] (_) -> Void in
                self?.dismiss(animated: true, completion: nil)
            }
            self.showToastWith(title: "Alert", message: error.localizedDescription, withAction: [failureResponse])
        }
        
        //get sat Info data
        apiService.fetchSatInfo(success:  {  [weak self] satInfoList in
            self?.SatInfoList = satInfoList
        }) { error in
            print(error)
            let failureResponse = UIAlertAction(title: "OK", style: .default) {[weak self] (_) -> Void in
                self?.dismiss(animated: true, completion: nil)
            }
            self.showToastWith(title: "Alert", message: error.localizedDescription, withAction: [failureResponse])

        }
    }
        
    //Get required Sat Info from selected school
    func getSatInfoForSchool(schoolName:String) -> SatInfo?  {
        print(schoolName.localizedLowercase)
        if let satInfo = self.SatInfoList.filter({$0.school_name.localizedLowercase == schoolName.localizedLowercase}) .first {
            return satInfo
        }
        return nil
    }
}

extension ViewController : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.InfoList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: nycCellIdentifier, for: indexPath) as! NYCTableViewCell
        
        let schoolInfo = self.InfoList[indexPath.row] as SchoolInfo
         cell.configureCell(with: schoolInfo)
        
        return cell
    }
}

extension ViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
         let schoolInfo = self.InfoList[indexPath.row] as SchoolInfo
        let reqSatInfo = getSatInfoForSchool(schoolName: schoolInfo.school_name ?? "")
        let detailVC = self.storyboard?.instantiateViewController(withIdentifier: Contants.Identifier.detailViewControllerIdentifier) as? DetailViewController
        detailVC?.satInfo = reqSatInfo
        self.navigationController?.pushViewController(detailVC!, animated: true)
    }
}
