//
//  ApiServices.swift
//  20190415-SivaramakrishnaSista-NYCSchools
//
//  Created by Sivaramakrishna Sista on 4/15/19.
//  Copyright © 2019 Sivaramakrishna Sista. All rights reserved.
//

import Foundation

open class ApiService: NSObject {
    var session: URLSession
    var dataTask: URLSessionDataTask?

    override init() {
        session = URLSession.shared
    }
    
    func fetchSchoolInfo(success _success:@escaping ([SchoolInfo]) -> Void, failure _failure:@escaping(Error) -> Void) {
        let success:([SchoolInfo]) -> Void = { SchoolInfoList in
            DispatchQueue.main.async {
                _success(SchoolInfoList)
            }
        }
        
        let failure:(Error) -> Void = { error in
            DispatchQueue.main.async {
                _failure(error)
            }
        }
        
        let request = NSMutableURLRequest(url: URL(string: Contants.URLS.SchoolInfo)! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        request.httpMethod = Contants.HttpMethod.GET.rawValue
        
        self.dataTask = self.session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
                failure(error!)
            }
            
            guard let data = data else {
                return
            }
                        
            // Parse JSON using JSONDecoder
            guard let nycSchoolList = try? JSONDecoder().decode([SchoolInfo].self, from: data) else {
                return
            }
            
            print(nycSchoolList.count)
            success(nycSchoolList)
        })
        
        dataTask?.resume()

    }

    
    func fetchSatInfo(success _success:@escaping ([SatInfo]) -> Void, failure _failure:@escaping(Error) -> Void) {
        let success:([SatInfo]) -> Void = { satInfoList in
            DispatchQueue.main.async {
                _success(satInfoList)
            }
        }
        
        let failure:(Error) -> Void = { error in
            DispatchQueue.main.async {
                _failure(error)
            }
        }
        
        let request = NSMutableURLRequest(url: URL(string: Contants.URLS.SatInfo)! ,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        request.httpMethod = Contants.HttpMethod.GET.rawValue
        
        self.dataTask = self.session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
                failure(error!)
            }
            
            guard let data = data else {
                return
            }
            
            // Parse JSON using JSONDecoder
            guard let nycSchoolList = try? JSONDecoder().decode([SatInfo].self, from: data) else {
                return
            }
            
            print(nycSchoolList.count)
            success(nycSchoolList)
        })
        
        dataTask?.resume()
        
    }
}
