//
//  SatInfo.swift
//  20190415-SivaramakrishnaSista-NYCSchools
//
//  Created by Sivaramakrishna Sista on 4/15/19.
//  Copyright © 2019 Sivaramakrishna Sista. All rights reserved.
//

import Foundation
struct SatInfo : Codable {
    let dbn : String
    let num_of_sat_test_takers : String
    let sat_critical_reading_avg_score : String
    let sat_math_avg_score : String
    let sat_writing_avg_score : String
    let school_name : String
    
    enum CodingKeys: String, CodingKey {
        
        case dbn = "dbn"
        case num_of_sat_test_takers = "num_of_sat_test_takers"
        case sat_critical_reading_avg_score = "sat_critical_reading_avg_score"
        case sat_math_avg_score = "sat_math_avg_score"
        case sat_writing_avg_score = "sat_writing_avg_score"
        case school_name = "school_name"
    }
}
