//
//  Extensions.swift
//  20190415-SivaramakrishnaSista-NYCSchools
//
//  Created by Sivaramakrishna Sista on 4/17/19.
//  Copyright © 2019 Sivaramakrishna Sista. All rights reserved.
//

import Foundation
import UIKit


extension UIViewController {
    public func showToastWith(title titleText:String , message messageText:String , withAction actionArray:[UIAlertAction]? ,withPrefferedStyle:UIAlertController.Style = .alert) {
        
        let alert = UIAlertController(title: titleText, message: messageText, preferredStyle: withPrefferedStyle)
        guard actionArray != nil else  {
            alert.addAction(
                UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            return
        }
        for action in actionArray! {
            alert.addAction(action)
        }
        self.present(alert, animated: true, completion: nil)
    }
}
