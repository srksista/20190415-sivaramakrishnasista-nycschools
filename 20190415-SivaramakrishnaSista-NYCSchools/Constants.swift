//
//  Constants.swift
//  20190415-SivaramakrishnaSista-NYCSchools
//
//  Created by Sivaramakrishna Sista on 4/17/19.
//  Copyright © 2019 Sivaramakrishna Sista. All rights reserved.
//

import Foundation

struct Contants {
    
    struct Title {
        static let mainViewTitle = "NYC School List"
    }
    
    struct URLS {
        static let  SchoolInfo = "https://data.cityofnewyork.us/resource/s3k6-pzi2.json"
        static let SatInfo = "https://data.cityofnewyork.us/resource/f9bf-2cp4.json"
    }
    
    enum HttpMethod: String {
        case GET = "GET"
        case POST = "POST"
    }
    
    struct NibName {
        static let nycTableViewCell = "NYCTableViewCell"
    }
    
    struct Identifier {
        static let nycTableViewCellIdentifier = "nycCell"
        static let detailViewControllerIdentifier  = "detailViewController"
    }
}
