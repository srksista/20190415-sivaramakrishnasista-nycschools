//
//  _0190415_SivaramakrishnaSista_NYCSchoolsTests.swift
//  20190415-SivaramakrishnaSista-NYCSchoolsTests
//
//  Created by Sivaramakrishna Sista on 4/15/19.
//  Copyright © 2019 Sivaramakrishna Sista. All rights reserved.
//

import XCTest
@testable import _0190415_SivaramakrishnaSista_NYCSchools

class _0190415_SivaramakrishnaSista_NYCSchoolsTests: XCTestCase {

    func testFetchSchoolInfoSuccessReturnsList() {
        let jsonData = "[{\"academicopportunities1\":\"Learning to Work, Student Council, Advisory Leadership, School Newspaper, Community Service Group, School Leadership Team, Extended Day/PM School, College Now\",\"academicopportunities2\":\"CAMBA, Diploma Plus, Medgar Evers College, Coney Island Genera on Gap, Urban Neighborhood Services, Coney Island Coalition Against Violence, I Love My Life Initiative, New York City Police Department\",\"academicopportunities3\":\"The Learning to Work (LTW) partner for Liberation Diploma Plus High School is CAMBA,\"zip\":\"11224\"}]".data(using: .utf8)
        let apiService = ApiService()
        let mockURLSession  = MockURLSession(data: jsonData, urlResponse: nil, errorResp: nil)
        apiService.session = mockURLSession
        let ListExpectation = expectation(description: "schoolInfoList")
        var schoolInfoResponse: [SchoolInfo]?
        
        apiService.fetchSchoolInfo(success:  {  schoolInfoList in
            schoolInfoResponse = schoolInfoList
            ListExpectation.fulfill()
        } ) { error in
            self.waitForExpectations(timeout: 5.0)
             XCTAssertNotNil(schoolInfoResponse)
        }
    }

    func testFetchSchoolInfoWhenResponseErrorReturnsError() {
        let apiService = ApiService()
        let error = NSError(domain: "error", code: 1234, userInfo: nil)
        let mockURLSession  = MockURLSession(data: nil, urlResponse: nil, errorResp: error)
        apiService.session = mockURLSession
        let errorExpectation = XCTestExpectation(description: "error")
        var errorResponse: Error?
        
        apiService.fetchSchoolInfo(success:  {  schoolInfoList in
            errorResponse = error
            errorExpectation.fulfill()
        } ) { error in
            self.waitForExpectations(timeout: 5.0)
            XCTAssertNotNil(errorResponse)
        }
    }
}



class MockURLSession: URLSession {
    var cachedUrl: URL?
    private var mockTask: MockTask
    init(data: Data?, urlResponse: URLResponse?, errorResp: Error?) {
        mockTask = MockTask(data: data, urlResponse: urlResponse, errorResp:errorResp)
        
        func dataTask(with url: URL, completionHandler:  @escaping(Data?, URLResponse?, Error?) -> Void) -> URLSessionDataTask {
            mockTask.completionHandler = completionHandler
            return mockTask
        }
    }
}

class MockTask: URLSessionDataTask {
    var completionHandler: ((Data?, URLResponse?, Error?) -> Void)?

    private var data: Data?
    private var urlResponse: URLResponse?
    private var errorResp: Error?
    
    
    init(data: Data?, urlResponse: URLResponse?, errorResp: Error?) {
   
        self.data = data
        self.urlResponse = urlResponse
        self.errorResp = errorResp
    }
    override func resume() {
        DispatchQueue.main.async {
            self.completionHandler?(self.data, self.urlResponse, self.errorResp)
        }
    }
}
